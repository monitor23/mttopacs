package com.mttopacs.demo.repository;

import com.mttopacs.demo.entity.PACS008File;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

public interface PACS008FileRepository extends MongoRepository<PACS008File,String> {
    Optional<PACS008File> findByMtId(String mtId);
    public List<PACS008File> findByTimestampBetween(Instant start, Instant end);

    List<PACS008File> findByStatus(String pendingStatus);
}
