package com.mttopacs.demo.repository;

import com.mttopacs.demo.entity.MTFile;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.time.Instant;
import java.util.List;

public interface MTFileRepository extends MongoRepository<MTFile, String> {
    public List<MTFile> findByMtHashCode(String mtHashCode);
    public List<MTFile> findByPdeFlag(boolean pdeFlag);
    public List<MTFile> findByTimestampBetween(Instant start, Instant end);
}
