package com.mttopacs.demo.repository;

import com.mttopacs.demo.entity.Config;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ConfigRepository extends MongoRepository<Config, String> {
}
