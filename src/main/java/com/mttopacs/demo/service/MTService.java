package com.mttopacs.demo.service;

import com.mttopacs.demo.dto.BarDTO;
import com.mttopacs.demo.dto.DoughnutDTO;
import com.mttopacs.demo.entity.MTFile;
import com.mttopacs.demo.entity.PACS008File;
import com.mttopacs.demo.exception.DuplicateMtException;
import com.mttopacs.demo.repository.MTFileRepository;
import com.mttopacs.demo.repository.PACS008FileRepository;
import com.mttopacs.demo.util.TransactionStatus;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.mttopacs.demo.util.FileUtils.readLineByLine;

@Service
public class MTService {

    private MTFileRepository mtFileRepository;
    private PACS008FileRepository pacsFileRepository;
    private static final Logger logger = LogManager.getLogger(MTService.class);

    public MTService(MTFileRepository mtFileRepository, PACS008FileRepository pacsFileRepository) {
        this.mtFileRepository = mtFileRepository;
        this.pacsFileRepository = pacsFileRepository;
    }

    public MTFile addMTFile(File file, boolean pdeFlag) throws IOException, DuplicateMtException {
        String content = readLineByLine(file.getAbsolutePath());
        MTFile mtFile = new MTFile(file.getName());
        mtFile = saveMT(pdeFlag, content, mtFile);

        return mtFile;
    }

    public MTFile saveMT(boolean pdeFlag, String content, MTFile mtFile) throws DuplicateMtException {
        mtFile.setMtHashCode(String.valueOf(content.hashCode()));
        mtFile.setTimestamp(Instant.from(ZonedDateTime.now(ZoneId.of("Europe/Brussels"))));
        mtFile.setMtFile(content);
        mtFile.setPdeFlag(pdeFlag);
        if(mtFileRepository.findByMtHashCode(mtFile.getMtHashCode()).isEmpty() || pdeFlag == true) {

            mtFile = mtFileRepository.insert(mtFile);
        }
        else{
            throw new DuplicateMtException("File content already exists");
        }
        return mtFile;
    }

    public DoughnutDTO getDoughnutData(){

         DoughnutDTO doughnutDTO = new DoughnutDTO();
         doughnutDTO.setPdeCount(mtFileRepository.findByPdeFlag(true).size());
         doughnutDTO.setNonPdeCount(mtFileRepository.findAll().size()-doughnutDTO.getPdeCount());
        return doughnutDTO;
    }

    public BarDTO getBarData(){
        BarDTO barDTO = new BarDTO();
        //Instant today = Instant.now();
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY,0);
        calendar.set(Calendar.MINUTE,0);
        calendar.set(Calendar.SECOND,0);
        calendar.set(Calendar.MILLISECOND,1);

        Date date = calendar.getTime();
        Instant today = date.toInstant();
        Instant previousDay = today;
        int mtFilesCount = mtFileRepository.findByTimestampBetween(today, Instant.now()).size();
        int rejectedPacsToday=0;
        int notValidatedPacsToday=0;
        List<PACS008File> todayPacs = pacsFileRepository.findByTimestampBetween(today, Instant.now());
        for(PACS008File pacs008File: todayPacs){
            if(TransactionStatus.REJECT.label.equals(pacs008File.getStatus()))
                rejectedPacsToday++;
            if(TransactionStatus.NOTVALIDATED.label.equals(pacs008File.getStatus()))
                notValidatedPacsToday++;
        }
        int todayPacsCount=todayPacs.size();


        populateStatistics(barDTO, mtFilesCount, notValidatedPacsToday, rejectedPacsToday, todayPacsCount);
        int indes=today.toString().indexOf("T");
        barDTO.getLabels().add(Instant.now().toString().substring(0,indes));

        for (int i = 0; i < 30; i++) {
            Instant yesterday = today.minus(i + 1, ChronoUnit.DAYS);
            mtFilesCount  = mtFileRepository.findByTimestampBetween(yesterday, previousDay).size();
            List<PACS008File> pacs008FileList= pacsFileRepository.findByTimestampBetween(yesterday, previousDay);
            int rejectedPacs=0;
            int notValidatedPacs=0;
            for(PACS008File pacs008File: pacs008FileList){
                if(TransactionStatus.REJECT.label.equals(pacs008File.getStatus()))
                    rejectedPacs++;
                if(TransactionStatus.NOTVALIDATED.label.equals(pacs008File.getStatus()))
                    notValidatedPacs++;
            }
            int pacsFilesCount  = pacs008FileList.size();

            populateStatistics(barDTO, mtFilesCount, notValidatedPacs, rejectedPacs, pacsFilesCount);

            int index=previousDay.toString().indexOf("T");
            barDTO.getLabels().add(previousDay.toString().substring(0,index));
            previousDay = yesterday;
        }

        return barDTO;
    }

    private void populateStatistics(BarDTO barDTO, int mtFilesCount, int notValidatedPacsCount, int rejectedPacs, int pacsFilesCount) {
        barDTO.getInvalidBic().add(rejectedPacs);
        barDTO.getNotValidated().add(notValidatedPacsCount);
        barDTO.getInvalidFormat().add(mtFilesCount - pacsFilesCount);
        barDTO.getValid().add(pacsFilesCount - notValidatedPacsCount - rejectedPacs);
    }

    public void deleteMtTx(Instant start, Instant end){
        List<MTFile> mtFiles = mtFileRepository.findByTimestampBetween(start, end);
        logger.info("Find for delete " + mtFiles.size()+ " tx");
        mtFileRepository.deleteAll(mtFiles);
    }
}
