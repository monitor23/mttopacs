package com.mttopacs.demo.service;

import com.mttopacs.demo.entity.PACS008File;
import com.prowidesoftware.swift.model.MessageDirection;
import com.prowidesoftware.swift.model.mt.AbstractMT;
import com.prowidesoftware.swift.model.mx.AbstractMX;
import com.prowidesoftware.swift.translations.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class MT202Service {
    private MTToMXTransformerService mtToMXTransformerService;

    private static final Logger logger = LogManager.getLogger(MT202Service.class);

    public MT202Service(MTToMXTransformerService mtToMXTransformerService) {
        this.mtToMXTransformerService = mtToMXTransformerService;
    }

    public PACS008File transform(MultipartFile file, boolean pdeFlag) throws Exception {
        String fileContent = new String(file.getBytes());
        final AbstractMT source = AbstractMT.parse(fileContent);

        TranslatorConfiguration translatorConfiguration = new TranslatorConfiguration().setDirection(MessageDirection.Input);

        // get a translator for the available equivalent MX, with the precondition check in the factory switched off
        Translator<AbstractMT, AbstractMX> t = TranslatorFactory.getTranslator(source, new TranslatorFactoryConfiguration().setPreferLatestVersions(true));
        String mxContent = null;
        // check the translator exists
        if (t != null) {

            // check preconditions
            List<PreconditionError> errors = t.preconditionsCheck(source);

            if (!errors.isEmpty()) {
                for (PreconditionError e : errors) {
                    logger.error(e.toString());
                }
            } else {
                AbstractMX mx = t.translate(source, translatorConfiguration);
                mxContent = mx.message();

            }
        }
        String fileSuffix = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss").format(new Date());
        String filename="pacs-009_"+fileSuffix+".xml";
        return mtToMXTransformerService.createAndStoreMTMXFiles(file.getOriginalFilename(), filename, pdeFlag, fileContent, mxContent);
    }
}
