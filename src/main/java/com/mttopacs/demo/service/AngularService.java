package com.mttopacs.demo.service;

import com.mttopacs.demo.dto.AngularDTO;
import com.mttopacs.demo.entity.MTFile;
import com.mttopacs.demo.entity.PACS008File;
import com.mttopacs.demo.repository.MTFileRepository;
import com.mttopacs.demo.repository.PACS008FileRepository;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class AngularService {
    private PACS008FileRepository pacs008FileRepository;
    private MTFileRepository mtFileRepository;

    public AngularService(PACS008FileRepository pacs008FileRepository, MTFileRepository mtFileRepository) {
        this.pacs008FileRepository = pacs008FileRepository;
        this.mtFileRepository = mtFileRepository;
    }

    public List<AngularDTO> getAll() {

        List<MTFile> mtFiles = mtFileRepository.findAll(Sort.by(Sort.Direction.DESC, "timestamp"));
        List<AngularDTO> angularDTOList = new ArrayList<>();
        for (MTFile mtFile : mtFiles) {
            Optional<PACS008File> pacs008File = pacs008FileRepository.findByMtId(mtFile.getId());
            AngularDTO angularDTO = new AngularDTO();
            if (pacs008File.isPresent()) {
                PACS008File pacsElement = pacs008File.get();
                angularDTO.setPacsCreatedDate(pacsElement.getTimestamp());
                angularDTO.setPacsFileContent(pacsElement.getPacs008File());
                angularDTO.setPacsFileName(pacsElement.getFileName());
                angularDTO.setPacsStatus(pacsElement.getStatus());
                angularDTO.setRejectedReason(pacsElement.getRejectedReason());

            }else{
                angularDTO.setPacsFileName("Invalid MT");
            }
            angularDTO.setMtCreatedDate(mtFile.getTimestamp());
            angularDTO.setMtFileContent(mtFile.getMtFile());
            angularDTO.setMtFileName(mtFile.getFileName());
            angularDTO.setPdeFlag(mtFile.isPdeFlag());
            angularDTOList.add(angularDTO);
        }
        return angularDTOList;
    }
}
