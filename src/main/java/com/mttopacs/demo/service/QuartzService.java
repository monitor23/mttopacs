package com.mttopacs.demo.service;

import com.mttopacs.demo.dto.ValidatorResponse;
import com.mttopacs.demo.entity.Config;
import com.mttopacs.demo.entity.MTFile;
import com.mttopacs.demo.entity.PACS008File;
import com.mttopacs.demo.exception.DuplicateMtException;
import com.mttopacs.demo.util.TransactionStatus;
import com.prowidesoftware.swift.model.mt.mt5xx.MT564;
import com.prowidesoftware.swift.model.mx.MxSeev03100110;
import com.prowidesoftware.swift.model.mx.MxSeev03900202;
import com.prowidesoftware.swift.model.mx.MxSeev03900210;
import com.prowidesoftware.swift.translations.LogicalMessageCriteriaException;
import com.prowidesoftware.swift.translations.MT564_MxSeev03100110_Translation;
import com.prowidesoftware.swift.translations.MT564_MxSeev03900202_Translation;
import com.prowidesoftware.swift.translations.TranslationPreconditionException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static com.mttopacs.demo.util.FileUtils.moveFile;

@Component
public class QuartzService {
    private Runtime r = Runtime.getRuntime();
    private File jarPath;
    private File inputFilesPath;
    private String outPath;
    private String processedFilesPath;
    private String rejectedFilesPath;
    private MTService mtService;
    private PACSService pacsService;
    private ConfigService configService;
    private ValidationService validationService;

    private static final Logger logger = LogManager.getLogger(QuartzService.class);


    public QuartzService(Environment env, MTService mtService, PACSService pacsService, ConfigService configService, ValidationService validationService) {
        this.configService = configService;
        Config  config = configService.getConfig();
        jarPath = new File(env.getProperty("input.jars.path"));
        inputFilesPath = new File(config.getInputFolder());
        outPath = config.getOutputFolder();
        processedFilesPath = env.getProperty("processed.files.path");
        rejectedFilesPath = env.getProperty("rejected.files.path");
        this.mtService = mtService;
        this.pacsService = pacsService;
        this.validationService = validationService;
    }

    @Scheduled(fixedDelay = 1000)
    public void validatePendingTransactions() {
        List<PACS008File> pacsFileList = pacsService.findPendingTransactions();
        for (PACS008File pacsFile : pacsFileList) {
            if (pacsFile.getRetriesNo()!= null && pacsFile.getRetriesNo() > 0) {
                try {
                    ValidatorResponse validatorResponse = validationService.validate(pacsFile);
                    if (validatorResponse.isValid()) {
                        pacsFile.setStatus(TransactionStatus.OK.label);
                    } else {
                        pacsFile.setStatus(TransactionStatus.REJECT.label);
                        pacsFile.setRejectedReason(validatorResponse.getMessage());
                    }
                } catch (Exception e) {
                    logger.error(e.getMessage());
                } finally {
                    pacsFile.setRetriesNo(pacsFile.getRetriesNo() - 1);
                    pacsService.update(pacsFile);
                }
            }
            else{
                pacsFile.setStatus(TransactionStatus.NOTVALIDATED.label);
                pacsService.update(pacsFile);
            }
        }

    }

    @Scheduled(fixedDelay = 1000)
    public void scheduleFixedDelayTask() throws Exception {

        Process p = null;
        try {

            File[] matches = inputFilesPath.listFiles(new FilenameFilter() {
                public boolean accept(File inputFilesPath, String name) {
                    return name.startsWith("MT") && name.endsWith(".txt");
                }
            });
            if (matches == null) {
          //      throw new Exception("Incorrect file path");
            }
            File matchWithError;
            for (File match : matches) {
                String fileSuffix = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss").format(new Date());
                String fileName = match.getName().replaceAll("[.][^.]+$", fileSuffix+".xml");
                fileName = fileName.replace("input", "output");
                fileName = fileName.replace("MT", "PACS008");
                p = r.exec("java -cp * com.netlink.mt_pacs.MTtoPacs -n MT103ToPacs008 -i " + match.getPath() + " -o " + outPath +
                        fileName, null, jarPath);
                BufferedReader error = new BufferedReader(new InputStreamReader(p.getErrorStream()));
                String errorMessage = error.readLine();
                while (errorMessage != null) {
                    logger.info(errorMessage);
                    errorMessage = error.readLine();
                }
                MTFile mtFile = null;
                try {
                    mtFile = mtService.addMTFile(match, false);
                } catch (Exception e) {
                    logger.error(e.getMessage());
                    moveFile(match, rejectedFilesPath);
                }

                pacsService.addPACS008File(fileName, mtFile);
                moveFile(match, processedFilesPath);
            }

        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        finally {
            if(p!= null)
                p.destroy();
        }
    }

    public PACS008File scheduleFixedDelayTask(String payload) throws Exception {

        PACS008File pacs008File = null;

        String fileSuffix = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss").format(new Date());
        File mtFile = new File(processedFilesPath + "MT103Rest" + fileSuffix + ".txt");
        BufferedWriter writer = new BufferedWriter(new FileWriter(mtFile.getAbsolutePath()));
        writer.write(payload);
        writer.close();
        return getPacs008File(mtFile, false);

    }

    public PACS008File scheduleFixedDelayTask(MultipartFile uploadedFile, boolean pdeFlag) throws Exception {

        File convFile = new File(System.getProperty("java.io.tmpdir")+"/"+uploadedFile.getOriginalFilename());
        uploadedFile.transferTo(convFile);

        return getPacs008File(convFile,pdeFlag);
    }

    public PACS008File getPacs008File(File convFile, boolean pdeFlag) throws Exception {
        PACS008File pacs008File = null;

        String fileSuffix = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss").format(new Date());
        String outputFileName = "PACS008" + fileSuffix + ".xml";
        String outputPath = outPath + outputFileName;
        Process p3 = r.exec("java -cp * com.netlink.mt_pacs.MTtoPacs -n MT103ToPacs008 -i " + convFile.getAbsolutePath() + " -o " + outputPath, null, jarPath);
        BufferedReader error = new BufferedReader(new InputStreamReader(p3.getErrorStream()));
        String errorMessage = error.readLine();
        while (errorMessage != null) {
            logger.info(errorMessage);
            errorMessage = error.readLine();
        }
        if(p3 != null)
         p3.destroy();
        MTFile storedMtFile = null;
        try {
            storedMtFile = mtService.addMTFile(convFile, pdeFlag);
        } catch (DuplicateMtException e) {
            logger.error(e.getMessage());
            throw new Exception(e.getMessage());
        }

        pacs008File = pacsService.addPACS008File(outputFileName, storedMtFile);
        return pacs008File;
    }
}
