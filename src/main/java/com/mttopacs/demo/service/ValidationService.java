package com.mttopacs.demo.service;


import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mttopacs.demo.dto.ValidatorResponse;
import com.mttopacs.demo.entity.PACS008File;
import io.micrometer.core.instrument.util.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tomcat.util.codec.binary.Base64;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ValidationService {

    private static final Logger logger = LogManager.getLogger(ValidationService.class);
    private String httpBicValidator;
    private String user;
    private String password;
    private String userToken;
    private String passwordToken;
    private String tokenPath;
    private String bicPath;

    public ValidationService(Environment environment) {
        this.httpBicValidator = environment.getProperty("swift.url");
        this.user = environment.getProperty("swift.user");
        this.password = environment.getProperty("swift.password");
        this.userToken = environment.getProperty("swift.token.user");
        this.passwordToken = environment.getProperty("swift.token.pass");
        this.tokenPath = environment.getProperty("swift.token.path");
        this.bicPath = environment.getProperty("swift.bic.path");
    }

    public ValidatorResponse validate(PACS008File pacs008File) {
        ValidatorResponse validatorResponse = new ValidatorResponse();
        validatorResponse.setValid(false);

        for (String bic : extractValue(pacs008File.getPacs008File(), "BICFI")) {
            extracted(validatorResponse, bic);
        }
        for (String bic : extractValue(pacs008File.getPacs008File(), "AnyBIC")) {
            extracted(validatorResponse, bic);
        }
        return validatorResponse;
    }

    public String getAccessToken() {
        HttpResponse<String> responseU = null;
        HttpResponse<JsonNode> jsonResponse = null;
        String credentials = userToken + ":" + passwordToken;
        String encodedCredentials = new String(Base64.encodeBase64(credentials.getBytes()));
        Map<String, String> headers = new HashMap<>();
        headers.put("Authorization", "Basic " + encodedCredentials);
        headers.put("content-type", "application/x-www-form-urlencoded");
        try {
            jsonResponse = Unirest.post(httpBicValidator + tokenPath)
                    .headers(headers)
                    .body("grant_type=password&username=" + user + "&password=" + password)
                    .asJson();

        } catch (UnirestException e) {
            e.printStackTrace();
        }
        return jsonResponse.getBody().getObject().getString("access_token");
    }

    private void extracted(ValidatorResponse validatorResponse, String bic) {
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {

        }

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", "Bearer " + getAccessToken());
        HttpEntity entity = new HttpEntity(headers);
        if(StringUtils.isNotEmpty(bic)) {
            try {
                ResponseEntity<Object> response = restTemplate.exchange(httpBicValidator + bicPath + bic, HttpMethod.GET, entity, Object.class);
                validatorResponse.setValid(true);
            } catch (HttpClientErrorException e) {
                validatorResponse.setValid(false);
                if (StringUtils.isEmpty(validatorResponse.getMessage())) {
                    validatorResponse.setMessage(e.getStatusText() + ": "+ bic);
                } else
                    validatorResponse.setMessage(validatorResponse.getMessage() + bic + " ");
            }
        }
    }

    public List<String> extractValue(String xmlPayload, String tagName) {
        List<String> ibans = new ArrayList<>();
        while (StringUtils.isNotEmpty(xmlPayload)) {
            if (xmlPayload.contains(tagName)) {
                String tagValue = null;
                int startIndexOfTagName = xmlPayload.indexOf("<" + tagName + ">");
                int endIndexOfTagName = xmlPayload.indexOf("</" + tagName + ">");
                int startIndexOfTagNameValue = startIndexOfTagName + tagName.length() + 2;
                if (startIndexOfTagNameValue != endIndexOfTagName) {
                    tagValue = xmlPayload.substring(startIndexOfTagNameValue, endIndexOfTagName);
                    ibans.add(tagValue);
                    xmlPayload = xmlPayload.substring(endIndexOfTagName + 5);
                } else {
                    xmlPayload = "";
                }
            } else xmlPayload = "";
        }

        return ibans;

    }

    public JSONObject getBicDetails(String bic) {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", "Bearer " + getAccessToken());
        // headers.set("Content-Type", "application/json ");
        ResponseEntity<String> response = null;
        JSONParser parser = new JSONParser();
        JSONObject json = null;

        HttpEntity entity = new HttpEntity(headers);
        try {
            response = restTemplate.exchange(httpBicValidator + bicPath + bic, HttpMethod.GET, entity, String.class);
        } catch (HttpClientErrorException e) {
            logger.error(e.getMessage());
            json = new JSONObject();
            json.put("message", "BIC not found in Swift system");

            return json;
        }

        try {
            json = (JSONObject) parser.parse(response.getBody());
        } catch (ParseException e) {
            logger.error(e.getMessage());
        }
        return json;
    }
}
