package com.mttopacs.demo.service;

import com.mttopacs.demo.entity.MTFile;
import com.mttopacs.demo.entity.PACS008File;
import com.mttopacs.demo.exception.DuplicateMtException;
import com.prowidesoftware.swift.model.mt.mt5xx.MT564;
import com.prowidesoftware.swift.model.mx.MxSeev03100110;
import com.prowidesoftware.swift.translations.LogicalMessageCriteriaException;
import com.prowidesoftware.swift.translations.MT564_MxSeev03100110_Translation;
import com.prowidesoftware.swift.translations.TranslationPreconditionException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class MT564Service {

    private  MTService mtService;
    private PACSService pacsService;
    private MTToMXTransformerService mtToMXTransformerService;
    private static final Logger logger = LogManager.getLogger(MT564Service.class);

    public MT564Service(MTService mtService, PACSService pacsService, MTToMXTransformerService mtToMXTransformerService) {
        this.mtService = mtService;
        this.pacsService = pacsService;
        this.mtToMXTransformerService = mtToMXTransformerService;
    }

    public PACS008File transform(MultipartFile uploadedFile, boolean pdeFlag) throws Exception {
        /*
         * Parse the source message
         */
        String fileContent = new String(uploadedFile.getBytes());
        MT564 source = MT564.parse(fileContent);

        /*
         * Create an instance of the proper translator
         */
        MT564_MxSeev03100110_Translation translator = new MT564_MxSeev03100110_Translation();
        MxSeev03100110 mx = null;
        String mxContent= null;
        try {
            mx = (MxSeev03100110) translator.translate(source);
            mxContent = mx.message();
        } catch (final LogicalMessageCriteriaException e1) {
            logger.error(e1.getMessage());
        } catch (final TranslationPreconditionException e2) {
            logger.error(e2.getMessage());
        }
        String fileSuffix = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss").format(new Date());
        String filename="seev.031.01.10_"+fileSuffix+".xml";
        return mtToMXTransformerService.createAndStoreMTMXFiles(uploadedFile.getOriginalFilename(), filename, pdeFlag, fileContent, mxContent);
    }


}
