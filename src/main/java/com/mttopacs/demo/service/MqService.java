package com.mttopacs.demo.service;

import com.ibm.mq.*;
import com.ibm.mq.constants.CMQC;
import com.ibm.mq.constants.CMQCFC;
import com.ibm.mq.headers.MQDataException;
import com.ibm.mq.headers.pcf.PCFMessage;
import com.ibm.mq.headers.pcf.PCFMessageAgent;
import com.mttopacs.demo.dto.QueueDTO;
import com.mttopacs.demo.entity.Config;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;


@Service
public class MqService {

    private String inQueue;
    private static final Logger logger = LogManager.getLogger(MqService.class);
    private final char CR = (char) 0x0D;
    private ConfigService configService;
    private PCFMessageAgent agent;
    private MQQueueManager mqQueueManager;
    private Environment environment;
    private String[] allQueues;

    public MqService(ConfigService configService, Environment environment) {
        this.configService = configService;
        this.environment = environment;
        allQueues = environment.getProperty("mq.allQueues").split(",");
    }

    public MQQueueManager getMqQueueManager() throws MQDataException, MQException {

        Config config = configService.getConfig();
        String host = config.getMqHost();
        int port = Integer.parseInt(config.getMqPort());
        String channel = config.getMqChannel();
        MQEnvironment.hostname = host;
        MQEnvironment.port = port;
        MQEnvironment.channel = channel;
        inQueue = config.getInQueue();
        agent = null;
        agent = new PCFMessageAgent();
        agent.connect(host, port, channel);
        mqQueueManager = new MQQueueManager(agent.getQManagerName());
        return mqQueueManager;
    }

    public String readContent(MultipartFile uploadedFile) throws IOException {

        InputStream stream = uploadedFile.getInputStream();
        InputStreamReader isReader = new InputStreamReader(stream);
        BufferedReader reader = new BufferedReader(isReader);
        String output = "";
        String line;
        int i = 0;

        while ((line = reader.readLine()) != null) {
            if (i == 0) {
                output = String.valueOf(output) + line;
                i++;
            } else if (i == 1) {
                output = String.valueOf(output) + CR + "\n" + line;
            }
        }
        return output;
    }

    public void putInQueue(String content, boolean isDummy) throws MQException, IOException, MQDataException {
        String inMessage = content;
        mqQueueManager = getMqQueueManager();
        MQMessage msg = new MQMessage();
        msg.characterSet = 1208;
        MQPutMessageOptions pmo = new MQPutMessageOptions();
        msg.write(inMessage.getBytes());
        mqQueueManager.put(inQueue, msg, pmo);
        mqQueueManager.close();
        mqQueueManager.disconnect();
        logger.info("Message was put with in " + inQueue);
    }
/*
    code from swift tool
 */
    public List<QueueDTO> viewMQDetails() {
        PCFMessage[] responses = null;

        List<QueueDTO> queues = new ArrayList<QueueDTO>();
        try {
            MQQueueManager qm = getMqQueueManager();

            PCFMessage request = null;

            for (String inQueueElement : allQueues) {


                request = new PCFMessage(CMQCFC.MQCMD_INQUIRE_Q);
                /**
                 * You can explicitly set a queue name like "TEST.Q1" or use a wild card like
                 * "TEST.*"
                 */
                request.addParameter(CMQC.MQCA_Q_NAME, inQueueElement);
                // Add parameter to request only local queues
                request.addParameter(CMQC.MQIA_Q_TYPE, CMQC.MQQT_LOCAL);
                // Add parameter to request only queue name and current depth
                request.addParameter(CMQCFC.MQIACF_Q_ATTRS,
                        new int[]{CMQC.MQCA_Q_NAME, CMQC.MQIA_CURRENT_Q_DEPTH, CMQC.MQIA_MAX_Q_DEPTH,
                                CMQC.MQIA_OPEN_INPUT_COUNT, CMQC.MQIA_OPEN_OUTPUT_COUNT,
                                CMQC.MQIA_MAX_MSG_LENGTH, CMQC.MQIA_DEF_PERSISTENCE});

                responses = agent.send(request);

                for (int i = 0; i < responses.length; i++) {
                    QueueDTO queue = new QueueDTO();
                    String queueName = "";
                    if (((responses[i]).getCompCode() == CMQC.MQCC_OK) && ((responses[i]).getParameterValue(CMQC.MQCA_Q_NAME) != null)) {
                        queueName = responses[i].getStringParameterValue(CMQC.MQCA_Q_NAME).trim();
                        if ((!queueName.contains("AMQ.")) && (!queueName.contains("SYSTEM."))) {
                            queue = new QueueDTO();
                            queue.name = queueName;
                            int depth = responses[i].getIntParameterValue(CMQC.MQIA_CURRENT_Q_DEPTH);
                            queue.curdepth = depth;
                            if (depth > 0) {
                                if (depth > 10) {
                                    queue.clase = "cell_danger";
                                } else {
                                    queue.clase = "cell_warning";
                                }
                            }
                            queue.maxdepth = responses[i].getIntParameterValue(CMQC.MQIA_MAX_Q_DEPTH);
                            queue.inputc = responses[i].getIntParameterValue(CMQC.MQIA_OPEN_INPUT_COUNT);
                            queue.outputc = responses[i].getIntParameterValue(CMQC.MQIA_OPEN_OUTPUT_COUNT);
                            queue.maxmsgl = responses[i].getIntParameterValue(CMQC.MQIA_MAX_MSG_LENGTH);
                            int defpsist = responses[i].getIntParameterValue(CMQC.MQIA_DEF_PERSISTENCE);
                            // 1=SI
                            if (defpsist == 1) {
                                queue.defpsist = "YES";
                            } else {
                                queue.defpsist = "NO";
                            }
                            queues.add(queue);
                        }
                    }
                }
            }
            qm.disconnect();
        } catch (MQException e) {
            logger.error(e.getMessage());

        } catch (MQDataException e) {
            logger.error(e.getMessage());
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
        return queues;
    }

}