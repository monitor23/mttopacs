package com.mttopacs.demo.service;

import com.mttopacs.demo.entity.Config;
import com.mttopacs.demo.entity.MTFile;
import com.mttopacs.demo.entity.PACS008File;
import com.mttopacs.demo.repository.PACS008FileRepository;
import com.mttopacs.demo.util.TransactionStatus;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.io.File;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;

import static com.mttopacs.demo.util.FileUtils.readLineByLine;

@Service
public class PACSService {

    private PACS008FileRepository pacs008FileRepository;
    private String outPath;
    private static final Logger logger = LogManager.getLogger(PACSService.class);
    private ConfigService configService;

    public PACSService(Environment env, PACS008FileRepository pacs008FileRepository, ConfigService configService) {
        this.pacs008FileRepository = pacs008FileRepository;
        this.configService = configService;
         this.outPath = configService.getConfig().getOutputFolder();
    }

    public PACS008File addPACS008File(String fileName, MTFile mtFile) {
        File outputFile = new File(outPath+fileName);
        if(outputFile.exists()) {
            String content = readLineByLine(outputFile.getAbsolutePath());
            PACS008File pacs008File = new PACS008File(outputFile.getName());
            pacs008File.setTimestamp(Instant.from(ZonedDateTime.now(ZoneId.of("Europe/Brussels"))));
            pacs008File.setPacs008File(content);
            pacs008File.setMtId(mtFile.getId());
            pacs008File.setStatus(TransactionStatus.PEND.label);
            if( configService.getConfig()!= null && configService.getConfig().getRetriesNo()!= null) {
                pacs008File.setRetriesNo(Integer.valueOf(configService.getConfig().getRetriesNo()));
            }
            else
                pacs008File.setRetriesNo(1);
            pacs008File = pacs008FileRepository.insert(pacs008File);
            return pacs008File;
        }
        else{
            logger.error("Transformation failed - PACS file was not generated");
            return null;
        }
    }

    public void deletePacsTx(Instant startInstant, Instant endInstant) {
        List<PACS008File> pacs008Files = pacs008FileRepository.findByTimestampBetween(startInstant, endInstant);
        pacs008FileRepository.deleteAll(pacs008Files);
    }

    public List<PACS008File> findPendingTransactions(){
        return pacs008FileRepository.findByStatus(TransactionStatus.PEND.label);
    }

    public void update(PACS008File pacsFile) {
        pacs008FileRepository.save(pacsFile);
    }

    public PACS008File addPACS008FileFromMT(String content, MTFile mtFile, String filename) {

            PACS008File pacs008File = new PACS008File(filename);
            pacs008File.setTimestamp(Instant.from(ZonedDateTime.now(ZoneId.of("Europe/Brussels"))));
            pacs008File.setPacs008File(content);
            pacs008File.setMtId(mtFile.getId());
            pacs008File.setStatus(TransactionStatus.PEND.label);
            if( configService.getConfig()!= null && configService.getConfig().getRetriesNo()!= null) {
                pacs008File.setRetriesNo(Integer.valueOf(configService.getConfig().getRetriesNo()));
            }
            else
                pacs008File.setRetriesNo(1);
            pacs008File = pacs008FileRepository.insert(pacs008File);
            return pacs008File;
    }

}
