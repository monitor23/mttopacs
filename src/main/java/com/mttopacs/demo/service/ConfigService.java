package com.mttopacs.demo.service;

import com.mttopacs.demo.entity.Config;
import com.mttopacs.demo.repository.ConfigRepository;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;


@Service
public class ConfigService {
    private ConfigRepository configRepository;
    private Environment environment;

    public ConfigService(ConfigRepository configRepository, Environment environment) {
        this.configRepository = configRepository;
        this.environment = environment;
    }

    @PostConstruct
    public void initDatabase(){
        if (configRepository.findAll().isEmpty()){
            Config config = new Config();
            config.setInputFolder(environment.getProperty("input.files.path"));
            config.setOutputFolder(environment.getProperty("output.files.path"));
            config.setMqChannel(environment.getProperty("mq.channel"));
            config.setMqHost(environment.getProperty("mq.hostname"));
            config.setMqPort(environment.getProperty("mq.port"));
            config.setMqName(environment.getProperty("mq.name"));
            config.setInQueue(environment.getProperty("mq.inQueue"));
            config.setRetriesNo(environment.getProperty("swift.retries"));
            configRepository.save(config);
        }

    }

    public Config getConfig() {
        return configRepository.findAll().get(0);
    }

    public void updateConfig(Config oldConfig) {
        configRepository.save(oldConfig);
    }
}
