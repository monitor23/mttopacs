package com.mttopacs.demo.service;

import com.mttopacs.demo.entity.MTFile;
import com.mttopacs.demo.entity.PACS008File;
import com.mttopacs.demo.exception.DuplicateMtException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
public class MTToMXTransformerService {
    private PACSService pacsService;
    private MTService mtService;
    private static final Logger logger = LogManager.getLogger(MTToMXTransformerService.class);

    public MTToMXTransformerService(PACSService pacsService, MTService mtService) {
        this.pacsService = pacsService;
        this.mtService = mtService;
    }

    public PACS008File createAndStoreMTMXFiles(String originalFilename, String filename, boolean pdeFlag, String fileContent, String mxContent) throws Exception {
        MTFile mtFile = null;
        try {
            mtFile = saveMTFromContent(fileContent,originalFilename, pdeFlag);

        } catch (DuplicateMtException e) {
            logger.error(e.getMessage());
            throw new Exception(e.getMessage());
        }
        if (StringUtils.hasText(mxContent)) {
            mxContent = mxContent.replace("h:", "");
            mxContent = mxContent.replace("Doc:", "");

            PACS008File pacs008File = pacsService.addPACS008FileFromMT(mxContent, mtFile, filename);

            return pacs008File;
        } else return null;
    }

    private MTFile saveMTFromContent(String content, String originalFilename, boolean pdeFlag) throws DuplicateMtException {

        MTFile mtFile = new MTFile(originalFilename);
        mtFile = mtService.saveMT(pdeFlag, content, mtFile);
        return mtFile;
    }
}
