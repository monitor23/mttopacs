package com.mttopacs.demo.exception;

public class DuplicateMtException extends Exception {

    public DuplicateMtException(String message) {
        super(message);
    }
}
