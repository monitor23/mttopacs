package com.mttopacs.demo.dto;

import java.util.ArrayList;
import java.util.List;

public class BarDTO {
    private List<String> labels = new ArrayList<>();
    private List<Integer> valid = new ArrayList<>();
    private List<Integer> invalidBic = new ArrayList<>();
    private List<Integer> invalidFormat = new ArrayList<>();
    private List<Integer> notValidated = new ArrayList<>();

    public List<String> getLabels() {
        return labels;
    }

    public void setLabels(List<String> labels) {
        this.labels = labels;
    }

    public List<Integer> getValid() {
        return valid;
    }

    public void setValid(List<Integer> valid) {
        this.valid = valid;
    }

    public List<Integer> getInvalidBic() {
        return invalidBic;
    }

    public void setInvalidBic(List<Integer> invalidBic) {
        this.invalidBic = invalidBic;
    }

    public List<Integer> getInvalidFormat() {
        return invalidFormat;
    }

    public void setInvalidFormat(List<Integer> invalidFormat) {
        this.invalidFormat = invalidFormat;
    }

    public List<Integer> getNotValidated() {
        return notValidated;
    }

    public void setNotValidated(List<Integer> notValidated) {
        this.notValidated = notValidated;
    }
}
