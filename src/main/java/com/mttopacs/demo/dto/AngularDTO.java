package com.mttopacs.demo.dto;

import java.time.Instant;

public class AngularDTO {

    private String mtFileName;
    private String mtFileContent;
    private Instant mtCreatedDate;
    private String pacsFileName;
    private String pacsFileContent;
    private Instant pacsCreatedDate;
    private boolean pdeFlag;
    private String pacsStatus;
    private String rejectedReason;

    public boolean isPdeFlag() {
        return pdeFlag;
    }

    public void setPdeFlag(boolean pdeFlag) {
        this.pdeFlag = pdeFlag;
    }

    public String getMtFileName() {
        return mtFileName;
    }

    public void setMtFileName(String mtFileName) {
        this.mtFileName = mtFileName;
    }

    public String getMtFileContent() {
        return mtFileContent;
    }

    public void setMtFileContent(String mtFileContent) {
        this.mtFileContent = mtFileContent;
    }

    public Instant getMtCreatedDate() {
        return mtCreatedDate;
    }

    public void setMtCreatedDate(Instant mtCreatedDate) {
        this.mtCreatedDate = mtCreatedDate;
    }

    public String getPacsFileName() {
        return pacsFileName;
    }

    public void setPacsFileName(String pacsFileName) {
        this.pacsFileName = pacsFileName;
    }

    public String getPacsFileContent() {
        return pacsFileContent;
    }

    public void setPacsFileContent(String pacsFileContent) {
        this.pacsFileContent = pacsFileContent;
    }

    public Instant getPacsCreatedDate() {
        return pacsCreatedDate;
    }

    public void setPacsCreatedDate(Instant pacsCreatedDate) {
        this.pacsCreatedDate = pacsCreatedDate;
    }

    public String getPacsStatus() {
        return pacsStatus;
    }

    public void setPacsStatus(String pacsStatus) {
        this.pacsStatus = pacsStatus;
    }

    public String getRejectedReason() {
        return rejectedReason;
    }

    public void setRejectedReason(String rejectedReason) {
        this.rejectedReason = rejectedReason;
    }
}
