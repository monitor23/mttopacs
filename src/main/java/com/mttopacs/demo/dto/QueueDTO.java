package com.mttopacs.demo.dto;

public class QueueDTO {
	public String name;
	public int curdepth;
	public int maxdepth;
	public int inputc;
	public int outputc;
	public int maxmsgl;
	public String defpsist;
	public String stats;
	public int enq;
	public int deq;
	public String time;
	public String clase; // se usa para el color de la tabla

	public QueueDTO(String name, int curdepth, int maxdepth,
                    int inputc, int outputc, int maxmsgl,
                    String defpsist, String stats, int enq, int deq, String time, String clase) {
		super();
		this.name = name;
		this.curdepth = curdepth;
		this.maxdepth = maxdepth;
		this.inputc = inputc;
		this.outputc = outputc;
		this.maxmsgl = maxmsgl;
		this.defpsist = defpsist;
		this.stats = stats;
		this.enq = enq;
		this.deq = deq;
		this.time = time;
		this.clase = clase;
	}

	public QueueDTO(){
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDefpsist() {
		return defpsist;
	}

	public void setDefpsist(String defpsist) {
		this.defpsist = defpsist;
	}

	public String getStats() {
		return stats;
	}

	public void setStats(String stats) {
		this.stats = stats;
	}
	
	public String getClase() {
		return clase;
	}

	public void setClase(String clase) {
		this.clase = clase;
	}


	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public int getCurdepth() {
		return curdepth;
	}

	public void setCurdepth(int curdepth) {
		this.curdepth = curdepth;
	}

	public int getMaxdepth() {
		return maxdepth;
	}

	public void setMaxdepth(int maxdepth) {
		this.maxdepth = maxdepth;
	}

	public int getInputc() {
		return inputc;
	}

	public void setInputc(int inputc) {
		this.inputc = inputc;
	}

	public int getOutputc() {
		return outputc;
	}

	public void setOutputc(int outputc) {
		this.outputc = outputc;
	}

	public int getMaxmsgl() {
		return maxmsgl;
	}

	public void setMaxmsgl(int maxmsgl) {
		this.maxmsgl = maxmsgl;
	}

	public int getEnq() {
		return enq;
	}

	public void setEnq(int enq) {
		this.enq = enq;
	}

	public int getDeq() {
		return deq;
	}

	public void setDeq(int deq) {
		this.deq = deq;
	}
	
	
}
