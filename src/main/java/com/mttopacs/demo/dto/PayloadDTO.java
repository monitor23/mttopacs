package com.mttopacs.demo.dto;

public class PayloadDTO {
    private String content;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
