package com.mttopacs.demo.dto;

public class DoughnutDTO {
    private int nonPdeCount;
    private int pdeCount;

    public int getNonPdeCount() {
        return nonPdeCount;
    }

    public void setNonPdeCount(int nonPdeCount) {
        this.nonPdeCount = nonPdeCount;
    }

    public int getPdeCount() {
        return pdeCount;
    }

    public void setPdeCount(int pdeCount) {
        this.pdeCount = pdeCount;
    }
}
