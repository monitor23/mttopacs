package com.mttopacs.demo.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.stream.Stream;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

public class FileUtils {
    private static final Logger logger = LogManager.getLogger(FileUtils.class);

    public static String readLineByLine(String filePath)
    {
        StringBuilder contentBuilder = new StringBuilder();

        try (Stream<String> stream = Files.lines( Paths.get(filePath), StandardCharsets.UTF_8))
        {
            stream.forEach(s -> contentBuilder.append(s).append("\n"));
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        return contentBuilder.toString();
    }

    public static void moveFile(File from, String to) throws IOException {
        String fileSuffix = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss").format(new Date());
        String newFileName = from.getName().replaceAll("[.][^.]+$", fileSuffix+".txt");
        Path src = Paths.get(from.getAbsolutePath());
        Path dest = Paths.get(to + newFileName);
        Path temp = Files.move(src, dest, REPLACE_EXISTING);
        if (temp != null) {
            logger.info("File was moved to "+dest.toString());
        } else {
            logger.error("Failed to move the file");
        }
    }
}
