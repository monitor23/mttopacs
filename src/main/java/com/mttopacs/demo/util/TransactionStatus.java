package com.mttopacs.demo.util;

public enum TransactionStatus {
    PEND("Pending"),
    REJECT("Rejected"),
    NOTVALIDATED("NotValidated"),
    OK("Verified");

    public final String label;
    private TransactionStatus(String label){
        this.label  = label;
    }

}
