package com.mttopacs.demo.util;

public class CreateMT199 {
    private final char CR  = (char) 0x0D;

    private String keypart1 = ""; //ABCD1234ABCD1234
    private String keypart2 = ""; //ABCD1234ABCD1234
    private String inQueue = "";
    private String qmgr = "";
    private String[] listQueues = null;
    private String block1="F01PTSQGBB0AAAA0003000003";
    private String block2="I999PTSQGBB0XAAAU3";
    private String block3="{108:robert}";
    private String block4_field20="test300";
    private String block4_field79="tests";

    public String createMT199() {
        String message = "{1:" + block1 + "}{2:" + block2 + "}{3:" + block3 + "}{4:" + CR + "\n:20:" + block4_field20 + CR + "\n:79:" + block4_field79 + CR + "\n-}";
    return message;
    }
}
