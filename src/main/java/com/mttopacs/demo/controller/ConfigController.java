package com.mttopacs.demo.controller;

import com.mttopacs.demo.dto.ResponseDTO;
import com.mttopacs.demo.entity.Config;
import com.mttopacs.demo.service.ConfigService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@CrossOrigin("*")
public class ConfigController {

    private ConfigService configService;

    public ConfigController(ConfigService configService) {
        this.configService = configService;
    }

    @GetMapping(value = "/getConfig", produces = MediaType.APPLICATION_JSON_VALUE)
    public Config getAll() { //ResponseEntity<ResponseDTO>
        Config config = configService.getConfig();
        return config;
    }

    @PostMapping(value = "/updateConfig", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDTO> handleFileUpload(@RequestBody Config config) throws IOException {
        Config oldConfig = configService.getConfig();
        oldConfig.setInputFolder(config.getInputFolder());
        oldConfig.setOutputFolder(config.getOutputFolder());
        oldConfig.setMqPort(config.getMqPort());
        oldConfig.setMqHost(config.getMqHost());
        oldConfig.setMqName(config.getMqName());
        oldConfig.setMqChannel(config.getMqChannel());
        oldConfig.setInQueue(config.getInQueue());
        oldConfig.setRetriesNo(config.getRetriesNo());

        configService.updateConfig(oldConfig);

        return new ResponseEntity(new ResponseDTO("Operation succeed"), HttpStatus.OK);
    }
}
