package com.mttopacs.demo.controller;

import com.ibm.mq.MQException;
import com.ibm.mq.headers.MQDataException;
import com.mttopacs.demo.dto.QueueDTO;
import com.mttopacs.demo.dto.ResponseDTO;
import com.mttopacs.demo.service.MqService;
import com.mttopacs.demo.util.CreateMT199;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@CrossOrigin("*")
public class MQController {
    private MqService mqService;
    private static final Logger logger = LogManager.getLogger(MQController.class);

    public MQController(MqService mqService) {
        this.mqService = mqService;
    }

    @PostMapping(value = "/uploadMQ", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDTO> handleFileUpload(@RequestParam("file") MultipartFile file) throws IOException {
            String content = mqService.readContent(file);
            try {
                mqService.putInQueue(content,true);
            } catch (MQException e) {
                logger.error(e.getMessage());
                return new ResponseEntity("Operation failed", HttpStatus.INTERNAL_SERVER_ERROR);
            } catch (MQDataException e) {
                logger.error(e.getMessage());
                return new ResponseEntity("Operation failed", HttpStatus.INTERNAL_SERVER_ERROR);
            }
        return new ResponseEntity(new ResponseDTO("Operation succeed"), HttpStatus.OK);
    }

    @GetMapping(value = "/uploadMQDummy", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDTO> handleFileUploadDummy() throws IOException {
            CreateMT199 createMT199= new CreateMT199();
            try {
                logger.info("Test message");
                mqService.putInQueue(createMT199.createMT199(),true);
            } catch (MQException e) {
                logger.error(e.getMessage());
                return new ResponseEntity("Operation failed", HttpStatus.INTERNAL_SERVER_ERROR);
            } catch (MQDataException e) {
                logger.error(e.getMessage());
                return new ResponseEntity("Operation failed", HttpStatus.INTERNAL_SERVER_ERROR);
            }
        return new ResponseEntity(new ResponseDTO("Operation succeed"), HttpStatus.OK);
    }

    @GetMapping(value = "/viewMQ", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<QueueDTO> viewMQ(){
      return   mqService.viewMQDetails();
    }

}
