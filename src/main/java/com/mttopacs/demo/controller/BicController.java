package com.mttopacs.demo.controller;

import com.mttopacs.demo.entity.Config;
import com.mttopacs.demo.service.ValidationService;
import org.json.simple.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.function.ServerResponse;

@RestController
@CrossOrigin("*")
public class BicController {
    ValidationService validationService;

    public BicController(ValidationService validationService) {
        this.validationService = validationService;
    }

    @GetMapping(value = "/bicDetails/{bic}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getBicDetails(@PathVariable (value="bic") String bic) { //ResponseEntity<ResponseDTO>

        JSONObject object = validationService.getBicDetails(bic);
        if(object != null) {

            return new ResponseEntity(object, HttpStatus.OK);
        }
        else{
            return new ResponseEntity("BIC not found in Swift system", HttpStatus.OK);
        }
    }
}
