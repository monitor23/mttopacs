package com.mttopacs.demo.controller;

import com.mongodb.client.MongoDatabase;
import com.mttopacs.demo.dto.ResponseDTO;
import com.mttopacs.demo.repository.MTFileRepository;
import com.mttopacs.demo.service.ConfigService;
import com.mttopacs.demo.service.MqService;
import org.springframework.core.env.Environment;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("*")
public class PingController {

    private MTFileRepository mtFileRepository;

    private MqService mqService;

    public PingController(MTFileRepository mtFileRepository, MqService mqService) {
        this.mtFileRepository = mtFileRepository;
        this.mqService = mqService;
    }

    @GetMapping(value = "/pingMongo", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity pingMongo() {
        try {
            mtFileRepository.findAll();
        } catch (Exception e) {

            return new ResponseEntity(new ResponseDTO(e.getMessage()), HttpStatus.OK);
        }

        return new ResponseEntity(new ResponseDTO("Ok"), HttpStatus.OK);
    }

    @GetMapping(value = "/pingMQ", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity pingMQ() {
        try {
            mqService.getMqQueueManager();
        } catch (Exception e) {

            return new ResponseEntity(new ResponseDTO(e.getMessage()), HttpStatus.OK);
        }

        return new ResponseEntity(new ResponseDTO("Ok"), HttpStatus.OK);
    }
}
