package com.mttopacs.demo.controller;

import com.mttopacs.demo.dto.AngularDTO;
import com.mttopacs.demo.dto.ResponseDTO;
import com.mttopacs.demo.entity.PACS008File;
import com.mttopacs.demo.service.AngularService;
import com.mttopacs.demo.service.MT202Service;
import com.mttopacs.demo.service.MT564Service;
import com.mttopacs.demo.service.QuartzService;
import com.prowidesoftware.swift.model.mt.mt5xx.MT564;
import com.prowidesoftware.swift.model.mx.MxSeev03900202;
import com.prowidesoftware.swift.translations.LogicalMessageCriteriaException;
import com.prowidesoftware.swift.translations.MT564_MxSeev03900202_Translation;
import com.prowidesoftware.swift.translations.TranslationPreconditionException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@CrossOrigin("*")
public class AngularController {
    private AngularService angularService;
    private QuartzService quartzService;
    private MT564Service mt564Service;
    private MT202Service mt202Service;


    public AngularController(AngularService angularService, QuartzService quartzService, MT564Service mt564Service, MT202Service mt202Service) {
        this.angularService = angularService;
        this.quartzService = quartzService;
        this.mt564Service = mt564Service;
        this.mt202Service = mt202Service;
    }

    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<AngularDTO> getAll() {
        return angularService.getAll();
    }

    @PostMapping(value = "/upload", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDTO> handleFileUpload(@RequestParam("file") MultipartFile file) {
        boolean pdeFlag = false;
        PACS008File pacs008File = null;
        String filename = file.getOriginalFilename().toLowerCase();
        if (filename.contains("mt564")) {
            try {
                pacs008File = mt564Service.transform(file, pdeFlag);
            } catch (Exception e) {
                return new ResponseEntity(new ResponseDTO(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        else if(filename.contains("mt202")){
            try {
                pacs008File = mt202Service.transform(file, pdeFlag);
            } catch (Exception e) {
                return new ResponseEntity(new ResponseDTO(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        else {
            try {
                pacs008File = quartzService.scheduleFixedDelayTask(file, pdeFlag);
            } catch (Exception e) {
                return new ResponseEntity(new ResponseDTO(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        if (pacs008File == null) {
            return new ResponseEntity(new ResponseDTO("Operation failed"), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity(new ResponseDTO(pacs008File.getFileName()), HttpStatus.OK);// pacs008File.getPacs008File();

    }

    @PostMapping(value = "/uploadDuplicate", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDTO> handleFileUploadDuplicate(@RequestParam("file") MultipartFile file) {
        boolean pdeFlag = true;
        PACS008File pacs008File = null;
        String filename = file.getOriginalFilename().toLowerCase();
        if (filename.contains("mt564")) {
            try {
                pacs008File = mt564Service.transform(file, pdeFlag);
            } catch (Exception e) {
                return new ResponseEntity(new ResponseDTO(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        else if(filename.contains("mt202")){
            try {
                pacs008File = mt202Service.transform(file, pdeFlag);
            } catch (Exception e) {
                return new ResponseEntity(new ResponseDTO(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        else {
            try {
                pacs008File = quartzService.scheduleFixedDelayTask(file, pdeFlag);
            } catch (Exception e) {
                return new ResponseEntity(new ResponseDTO(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        if (pacs008File == null) {
            return new ResponseEntity(new ResponseDTO("Operation failed"), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity(new ResponseDTO(pacs008File.getFileName()), HttpStatus.OK);// pacs008File.getPacs008File();
    }

}
