package com.mttopacs.demo.controller;

import com.mttopacs.demo.dto.PayloadDTO;
import com.mttopacs.demo.dto.ResponseDTO;
import com.mttopacs.demo.entity.PACS008File;
import com.mttopacs.demo.service.MTService;
import com.mttopacs.demo.service.QuartzService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.UUID;

@RestController
public class KafkaController {

    private QuartzService quartzService;
    private static final Logger logger = LogManager.getLogger(KafkaController.class);

    public KafkaController(QuartzService quartzService) {
        this.quartzService = quartzService;
    }

    @PostMapping(value = "/uploadBatch", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ResponseDTO> handleFileUpload(@RequestBody PayloadDTO payload) throws IOException {
        String fileSuffix = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS").format(new Date());
        String fileName = "MTfromKafkaTopic"+fileSuffix+".txt";
        File file = new File(fileName);
        file.createNewFile();
        logger.info(payload.getContent());
        try (PrintWriter out = new PrintWriter(fileName)) {
            out.write(payload.getContent());
        }
        boolean pdeFlag = true;
        PACS008File pacs008File = null;
        try {
            quartzService.getPacs008File(file, pdeFlag);
        } catch (Exception e) {
            return new ResponseEntity(new ResponseDTO(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (pacs008File == null) {
            return new ResponseEntity(new ResponseDTO("Operation failed"), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity(new ResponseDTO("Succes"), HttpStatus.OK);// pacs008File.getPacs008File();

    }
}
