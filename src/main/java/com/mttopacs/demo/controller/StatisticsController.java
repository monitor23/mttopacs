package com.mttopacs.demo.controller;

import com.mttopacs.demo.dto.BarDTO;
import com.mttopacs.demo.dto.DoughnutDTO;
import com.mttopacs.demo.service.MTService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
public class StatisticsController {
    private MTService mtService;

    public StatisticsController(MTService mtService) {
        this.mtService = mtService;
    }

    @GetMapping(value = "/pdeNonPde", produces = MediaType.APPLICATION_JSON_VALUE)
    public DoughnutDTO getDoughnutData() {
        return mtService.getDoughnutData();
    }

    @GetMapping(value = "/txByDay", produces = MediaType.APPLICATION_JSON_VALUE)
    public BarDTO getBarData() {
        return mtService.getBarData();
    }
}
