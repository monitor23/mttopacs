package com.mttopacs.demo.controller;

import com.mttopacs.demo.dto.PayloadDTO;
import com.mttopacs.demo.entity.PACS008File;
import com.mttopacs.demo.service.QuartzService;
import com.mttopacs.demo.service.ValidationService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class MTController {
    private QuartzService quartzService;
    private ValidationService validationService;
    private static final Logger logger = LogManager.getLogger(QuartzService.class);

    public MTController(QuartzService quartzService, ValidationService validationService) {
        this.quartzService = quartzService;
        this.validationService = validationService;
    }

    @PostMapping(value = "/mt", produces = MediaType.APPLICATION_XML_VALUE)
    @ResponseBody
    public ResponseEntity postResponseJsonContent(
            @RequestBody PayloadDTO payload) throws Exception {
        PACS008File pacs008File = null;
        try {
            pacs008File = quartzService.scheduleFixedDelayTask(payload.getContent());
        } catch (Exception e) {
            return new ResponseEntity(e.getMessage(),HttpStatus. INTERNAL_SERVER_ERROR);
        }
        if (pacs008File == null){
            return new ResponseEntity("Operation failed",HttpStatus. INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity(pacs008File.getPacs008File(), HttpStatus.OK);// pacs008File.getPacs008File();
    }

    @GetMapping(value = "/getToken", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String postResponseJsonContent() throws Exception {
        return validationService.getAccessToken();

    }
}
