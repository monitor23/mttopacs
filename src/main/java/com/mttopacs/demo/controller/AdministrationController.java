package com.mttopacs.demo.controller;

import com.mttopacs.demo.service.MTService;
import com.mttopacs.demo.service.PACSService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;

@RestController
@CrossOrigin("*")
public class AdministrationController {

    private MTService mtService;
    private PACSService pacsService;

    private static final Logger logger = LogManager.getLogger(AdministrationController.class);

    public AdministrationController(MTService mtService, PACSService pacsService) {
        this.mtService = mtService;
        this.pacsService = pacsService;
    }

    @GetMapping(value = "/deleteTx", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity deleteTx(@RequestParam String start, @RequestParam String end) {
        LocalDate sdate = LocalDate.parse(start);
        LocalDate edate = LocalDate.parse(end);
        Instant startInstant = sdate.atStartOfDay(ZoneId.systemDefault()).toInstant();
        Instant endInstant = edate.atStartOfDay(ZoneId.systemDefault()).toInstant();
        if (startInstant.equals(endInstant)) {
            endInstant = endInstant.plus(1, ChronoUnit.DAYS);
        }
        logger.info( "try to delete from " + start+ " until " +end);
        logger.info( "dates in utc are " +startInstant +" ->" + endInstant);

        mtService.deleteMtTx(startInstant, endInstant);
        pacsService.deletePacsTx(startInstant, endInstant);
        return new ResponseEntity( HttpStatus.OK);
    }
}
