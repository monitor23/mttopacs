package com.mttopacs.demo.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Instant;

@Document(collection = "mtFiles")
public class MTFile {
    @Id
    private String id;
    private String fileName;
    private String mtFile;
    private String mtHashCode;
    private Instant timestamp;
    private boolean pdeFlag;

    public boolean isPdeFlag() {
        return pdeFlag;
    }
    public void setPdeFlag(boolean pdeFlag) {
        this.pdeFlag = pdeFlag;
    }

    public String getMtHashCode() {
        return mtHashCode;
    }

    public void setMtHashCode(String mtHashCode) {
        this.mtHashCode = mtHashCode;
    }

    public MTFile(String fileName) {
        this.fileName = fileName;
    }

    public String getMtFile() {
        return mtFile;
    }

    public void setMtFile(String mtFile) {
        this.mtFile = mtFile;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Instant getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Instant timestamp) {
        this.timestamp = timestamp;
    }
}
