package com.mttopacs.demo.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "configs")
public class Config {
    @Id
    private String id;
    private String inputFolder;
    private String outputFolder;
    private String mqChannel;
    private String mqHost;
    private String mqPort;
    private String mqName;
    private String inQueue;
    private String retriesNo;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInputFolder() {
        return inputFolder;
    }

    public void setInputFolder(String inputFolder) {
        this.inputFolder = inputFolder;
    }

    public String getOutputFolder() {
        return outputFolder;
    }

    public void setOutputFolder(String outputFolder) {
        this.outputFolder = outputFolder;
    }

    public String getMqChannel() {
        return mqChannel;
    }

    public void setMqChannel(String mqChannel) {
        this.mqChannel = mqChannel;
    }

    public String getMqHost() {
        return mqHost;
    }

    public void setMqHost(String mqHost) {
        this.mqHost = mqHost;
    }

    public String getMqPort() {
        return mqPort;
    }

    public void setMqPort(String mqPort) {
        this.mqPort = mqPort;
    }

    public String getMqName() {
        return mqName;
    }

    public void setMqName(String mqName) {
        this.mqName = mqName;
    }

    public String getInQueue() {
        return inQueue;
    }

    public void setInQueue(String inQueue) {
        this.inQueue = inQueue;
    }

    public String getRetriesNo() {
        return retriesNo;
    }

    public void setRetriesNo(String retriesNo) {
        this.retriesNo = retriesNo;
    }
}
