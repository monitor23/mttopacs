package com.mttopacs.demo.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Instant;

@Document(collection = "pacs008Files")
public class PACS008File {
    @Id
    private String id;
    private String fileName;
    private String pacs008File;
    private Instant timestamp;
    private String mtId;
    private String status;
    private String rejectedReason;
    private Integer retriesNo;

    public String getMtId() {
        return mtId;
    }

    public void setMtId(String mtId) {
        this.mtId = mtId;
    }

    public PACS008File(String fileName) {
        this.fileName = fileName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Instant getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Instant timestamp) {
        this.timestamp = timestamp;
    }

    public String getPacs008File() {
        return pacs008File;
    }

    public void setPacs008File(String pacs008File) {
        this.pacs008File = pacs008File;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRejectedReason() {
        return rejectedReason;
    }

    public void setRejectedReason(String rejectedReason) {
        this.rejectedReason = rejectedReason;
    }

    public Integer getRetriesNo() {
        return retriesNo;
    }

    public void setRetriesNo(Integer retriesNo) {
        this.retriesNo = retriesNo;
    }
}
